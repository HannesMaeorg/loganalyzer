import helpers.Drawer;
import helpers.Histogram;
import javafx.util.Pair;
import model.Request;
import model.Resource;
import services.RequestService;
import services.ResourceService;

import java.time.LocalDateTime;
import java.util.List;

class LogAnalyzer {
    private long startTime = System.currentTimeMillis();

    void analyze(String path, int numberOfElements) {
        ResourceService resourceService = new ResourceService();
        RequestService requestService = new RequestService();

        List<Request> requests = requestService.getRequests(path);
        List<Resource> topResources = resourceService.getResources(requests, numberOfElements);
        List<Pair<LocalDateTime, Integer>> hourlyRequests = resourceService.getHourlyRequests(requests, topResources);
        dataPresenter(numberOfElements, topResources, hourlyRequests);
    }

    private void dataPresenter(int arg2, List<Resource> topResources, List<Pair<LocalDateTime, Integer>> hourlyRequests) {
        System.out.println();
        for (int i = 0; i < arg2; i++) {
            System.out.println(topResources.get(i));
        }
        System.out.println();
        List<String> histogram = Histogram.generateHistogram(hourlyRequests, 40);
        Drawer.drawHistogram(histogram);
        System.out.println();
        System.out.println("The program run for " + (System.currentTimeMillis() - startTime) + " milliseconds");
    }
}
