package model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Resource {

    private String resource;
    private double avgDuration;

    @Override
    public String toString() {
        return "resource= " + resource + ", avgDuration= " + String.format("%.2f", avgDuration) + " ms";
    }
}
