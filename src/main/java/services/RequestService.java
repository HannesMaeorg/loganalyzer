package services;

import dao.DataExtractor;
import model.Request;

import java.util.List;

public class RequestService {
    private DataExtractor dataExtractor = new DataExtractor();

    public List<Request> getRequests(String path) {
        return dataExtractor.extract(path);
    }
}
