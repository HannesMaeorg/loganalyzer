package services;

import javafx.util.Pair;
import model.Request;
import model.Resource;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.groupingBy;

public class ResourceService {

    public List<Resource> getResources(List<Request> requests, int n) {
        List<Resource> allResources = getAllResources(requests);
        allResources.sort(Comparator.comparing(Resource::getAvgDuration).reversed());
        return IntStream.range(0, n).mapToObj(allResources::get).collect(Collectors.toList());
    }

    public List<Pair<LocalDateTime, Integer>> getHourlyRequests(List<Request> requests, List<Resource> topResources) {
        List<Pair<LocalDateTime, Integer>> hourlyRequests = new ArrayList<>();

        LocalDateTime start = requests.get(0).getTime();
        LocalDateTime stop = requests.get(requests.size() - 1).getTime();
        for (LocalDateTime i = start; i.isBefore(stop.plusSeconds(1)); i = i.plusHours(1)) {
            int hour = i.getHour();
            List<Request> filterRequest = requests
                    .stream()
                    .filter(r -> r.getTime().getHour() == hour)
                    .collect(Collectors.toList());
            int numberOfRequest = findNumberOfResourceRequest(filterRequest, topResources);
            hourlyRequests.add(new Pair<>(LocalDateTime.of(i.getYear(), i.getMonth(), i.getDayOfMonth(), i.getHour(), 0), numberOfRequest));
        }
        return hourlyRequests;
    }

    private List<Resource> getAllResources(List<Request> requests) {
        Map<String, List<Request>> groupedRequests = requests.stream().collect(groupingBy(Request::getResource));
        List<Resource> resources = new ArrayList<>();
        groupedRequests.forEach((key1, list) -> {
            double average = list.stream().mapToDouble(Request::getDuration).sum() / (list.size());
            resources.add(Resource.builder()
                    .resource(key1)
                    .avgDuration(average)
                    .build());
        });
        return resources;
    }

    private int findNumberOfResourceRequest(List<Request> requests, List<Resource> topResources) {
        int number = 0;
        for (Request req : requests) {
            number += topResources.stream().filter(res -> req.getResource().equals(res.getResource())).count();
        }
        return number;
    }
}
