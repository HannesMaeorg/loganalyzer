package helpers;

import javafx.util.Pair;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static java.util.Comparator.comparing;

public class Histogram {
    public static List<String> generateHistogram(List<Pair<LocalDateTime, Integer>> hourlyRequests, int length) {
        double multiplier = getMultiplier(hourlyRequests, length);
        List<String> histogram = new ArrayList<>();
        for (Pair<LocalDateTime, Integer> r : hourlyRequests) {
            generateHistogramLine(multiplier, histogram, r);
        }
        return histogram;
    }

    private static void generateHistogramLine(double multiplier, List<String> histogram, Pair<LocalDateTime, Integer> r) {
        StringBuilder line = new StringBuilder(r.getKey().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + " | ");
        for (int i = 0; i < r.getValue() * multiplier; i++) {
            line.append("=");
        }
        if (r.getValue() != 0) {
            line.append(" ").append(r.getValue());
        }
        histogram.add(line.toString());
    }

    private static double getMultiplier(List<Pair<LocalDateTime, Integer>> hourlyRequests, double length) {
        int maxValue = hourlyRequests
                .stream()
                .max(comparing(Pair::getValue))
                .get()
                .getValue();
        return length / maxValue;
    }
}
