package helpers;

import model.Request;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class RequestMapper {
    public static Request map(String strLine) {

        strLine = strLine.trim();
        String[] parameters = strLine.split(" ");
        String dateTime = parameters[0] + " " + parameters[1];
        String uniqueResource = parameters[4].split("\\?")[0];
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS");

        return Request.builder()
                .time(LocalDateTime.parse(dateTime, formatter))
                .threadId(parameters[2])
                .userContext(parameters[3])
                .resource(uniqueResource)
                .duration(Long.parseLong(parameters[(parameters.length - 1)]))
                .build();
    }
}
