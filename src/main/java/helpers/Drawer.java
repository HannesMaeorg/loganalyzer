package helpers;

import java.util.Comparator;
import java.util.List;

public class Drawer {

    public static void drawHistogram(List<String> histogram) {
        drawTitles(histogram);
        for (String s : histogram) {
            System.out.println(s);
        }
    }

    private static void drawTitles(List<String> histogram) {
        String longestLine = histogram
                .stream()
                .max(Comparator.comparing(String::length))
                .orElse("");
        int maxLength = longestLine.length();

        String title = "Histogram of hourly number of requests";
        String rowTitles = "           Hours | Number of requests";
        System.out.println(title);
        printLines(maxLength);
        System.out.println(rowTitles);
        printLines(maxLength);
    }

    private static void printLines(int length) {
        for (int i = 0; i < length; i++) {
            System.out.print("-");
        }
        System.out.println();
    }
}
