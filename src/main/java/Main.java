public class Main {

    public static void main(String[] args) {

        LogAnalyzer logAnalyzer = new LogAnalyzer();

        if (args.length == 1) {
            if (args[0].equals("-h")) {
                System.out.println("-h          Shows this help message..");
                System.out.println("args[0]     First argument is the logfail name");
                System.out.println("args[1]     Second argument is requested number of top resources with highest average request duration");
            } else {
                throw new RuntimeException("Wrong argument, try -h");
            }
            return;
        }

        if (args.length == 2) {
            try {
                logAnalyzer.analyze(args[0], Integer.parseInt(args[1]));
            } catch (Exception e) {
                System.err.println("Error: " + e.getMessage());
            }
            return;
        }

        throw new RuntimeException("Wrong number of arguments");
    }
}
